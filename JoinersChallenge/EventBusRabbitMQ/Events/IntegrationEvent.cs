﻿using System;
using Newtonsoft.Json;

namespace EventBusRabbitMQ.Events
{
    public abstract class IntegrationEvent
    {
        [JsonProperty]
        public DateTime CreationDate { get; protected set; }

        [JsonProperty]
        public Guid Id { get; private set; }
        protected IntegrationEvent()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.UtcNow;
        }

        [JsonConstructor]
        protected IntegrationEvent(Guid id, DateTime createDate)
        {
            Id = id;
            CreationDate = createDate;
        }
    }
}
