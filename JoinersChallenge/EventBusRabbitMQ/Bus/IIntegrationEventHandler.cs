﻿using System.Threading.Tasks;
using EventBusRabbitMQ.Events;

namespace EventBusRabbitMQ.Bus
{
    public interface IIntegrationEventHandler<in TIntegrationEvent> : IIntegrationEventHandler
         where TIntegrationEvent : IntegrationEvent
    {
        Task Handle(TIntegrationEvent @event);
    }

    public interface IIntegrationEventHandler
    {
    }
}
