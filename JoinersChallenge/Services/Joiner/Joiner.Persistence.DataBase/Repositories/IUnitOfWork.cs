﻿using System.Diagnostics.CodeAnalysis;

namespace Joiner.Persistence.DataBase.Repositories
{
    
    public interface IUnitOfWork
    {
        IJoinerRespository JoinerRespository { get; }
        int Commit();
    }
}
