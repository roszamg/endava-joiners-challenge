﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Joiner.Persistence.DataBase.Repositories
{
    [ExcludeFromCodeCoverage]
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly JoinerContext _context;

        public IJoinerRespository JoinerRespository { get; }
        public UnitOfWork(
            JoinerContext context,
            IJoinerRespository joinerRespository)
        {
            _context = context;
            JoinerRespository = joinerRespository;
        }
        public int Commit()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

    }
}
