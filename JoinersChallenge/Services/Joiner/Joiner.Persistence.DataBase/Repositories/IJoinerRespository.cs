﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace Joiner.Persistence.DataBase.Repositories
{

    public interface IJoinerRespository
    {
        Domain.Joiner Add(Domain.Joiner joiner);

        void Update(Domain.Joiner joiner);

        Task<Domain.Joiner> GetJoinerAsync(int joinerId);

        ValueTask<Domain.Joiner> GetAllJoinerAsync();

        Task<Domain.Joiner> FindByIdentifacationNumber(int identificationNumber);

    }
}
