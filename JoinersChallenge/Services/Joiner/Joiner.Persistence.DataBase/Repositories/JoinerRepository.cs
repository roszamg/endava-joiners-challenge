﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Joiner.Persistence.DataBase.Repositories
{
    [ExcludeFromCodeCoverage]
    public class JoinerRepository : IJoinerRespository
    {
        private readonly JoinerContext _context;
        protected readonly DbSet<Domain.Joiner> _dbSet;
        public JoinerRepository(JoinerContext context)
        {
            _context = context;
            _dbSet = _context.Set<Domain.Joiner>();
        }

        public Domain.Joiner Add(Domain.Joiner joiner)
        {
            var joinerss = _context.Add(joiner).Entity;
            return joinerss;
        }

        public void Update(Domain.Joiner joiner)
        {
            _context.Entry(joiner).State = EntityState.Modified;
        }

        public async Task<Domain.Joiner> GetJoinerAsync(int joinerId)
        {
            var joiner = await _dbSet.FirstOrDefaultAsync(i => i.JoinerId == joinerId);

            return joiner;
        }

        public ValueTask<Domain.Joiner> GetAllJoinerAsync()
        {
            return _dbSet.FindAsync();

        }

        public Task<Domain.Joiner> FindByIdentifacationNumber(int identificationNumber)
        {
            var joiner = _dbSet.FirstOrDefaultAsync(c => c.IdentificationNumber == identificationNumber);
            return joiner;

        }
    }
}
