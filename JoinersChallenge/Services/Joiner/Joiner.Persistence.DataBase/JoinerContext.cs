﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace Joiner.Persistence.DataBase
{
    [ExcludeFromCodeCoverage]
    public class JoinerContext : DbContext
    {
        public JoinerContext(DbContextOptions<JoinerContext> options) : base(options)
        {

        }

        public DbSet<Domain.Joiner> Joiners { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Domain.Joiner>(entity =>
            {
                entity.HasIndex(x => x.JoinerId);
                entity.Property(x => x.Name).IsRequired();
            });           
        }
    }
}
