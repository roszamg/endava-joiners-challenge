﻿using System.Threading.Tasks;
using Joiner.API.Command;
using Joiner.API.Models;
using Joiner.API.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Joiner.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JoinerController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<JoinerController> _logger;
        public JoinerController(
            IMediator mediator, ILogger<JoinerController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet("{joinerId}")]
        public async Task<IActionResult> Get(int joinerId)
        {
            _logger.LogInformation("Get Joiner information");

            var query = new GetJoinerQuery(joinerId);
            var result = await _mediator.Send(query);

            return result != null ? (IActionResult)Ok(result) : NotFound();
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateJoiner([FromBody] JoinerDto joiner)
        {
            _logger.LogInformation("Create Joiner");
            var query = new CreateJoinerCommand(joiner);
            var result = await _mediator.Send(query);
            return CreatedAtAction(nameof(CreateJoiner), result);

        }

        [HttpPut("{joinerId}")]
        public async Task<IActionResult> Update(int joinerId, [FromBody] JoinerDto joiner)
        {
            _logger.LogInformation("Update Joiner");
            var query = new UpdateJoinerCommand(joiner, joinerId);
            var result = await _mediator.Send(query);

            return CreatedAtAction(nameof(Update), result);
        }
    }
}
