﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Joiner.API.Command;
using Joiner.API.IntegrationEvents;
using Joiner.API.IntegrationEvents.Events;
using Joiner.API.Models;
using Joiner.Persistence.DataBase.Repositories;
using MediatR;

namespace Joiner.API.Handlers
{
    public class CreateJoinerCommandHandler : IRequestHandler<CreateJoinerCommand, JoinerDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IJoinerIntegrationEventService _joinerIntegrationEventService;
        public CreateJoinerCommandHandler(IUnitOfWork unitOfWork,
            IMapper mapper,
            IJoinerIntegrationEventService joinerIntegrationEventService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _joinerIntegrationEventService = joinerIntegrationEventService;
        }

        public Task<JoinerDto> Handle(CreateJoinerCommand request, CancellationToken cancellationToken)
        {
            var joiner = _unitOfWork.JoinerRespository.Add(new Domain.Joiner()
            {
                Name = request.Joiner.Name,
                IdentificationNumber = request.Joiner.IdentificationNumber,
                DomainExperience = request.Joiner.DomainExperience,
                Language = request.Joiner.Language,
                Role = request.Joiner.Role,
                Stack = request.Joiner.Stack
            });

            _unitOfWork.Commit();

            var joinerEvent = new JoinerChangedIntegrationEvent(joiner.JoinerId, joiner.IdentificationNumber, joiner.Name, joiner.Stack, joiner.Role, joiner.Language, joiner.DomainExperience);

            _joinerIntegrationEventService.PublishThroughEventBusAsync(joinerEvent);

           

            return Task.FromResult(_mapper.Map<JoinerDto>(joiner));
        }
    }
}
