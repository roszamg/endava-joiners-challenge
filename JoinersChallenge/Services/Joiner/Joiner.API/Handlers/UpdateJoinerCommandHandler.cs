﻿using System.Threading;
using System.Threading.Tasks;
using Joiner.API.Command;
using Joiner.API.IntegrationEvents;
using Joiner.API.IntegrationEvents.Events;
using Joiner.Persistence.DataBase.Repositories;
using MediatR;

namespace Joiner.API.Handlers
{
    public class UpdateJoinerCommandHandler : IRequestHandler<UpdateJoinerCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IJoinerIntegrationEventService _joinerIntegrationEventService;
        public UpdateJoinerCommandHandler(
            IUnitOfWork unitOfWork,            
            IJoinerIntegrationEventService joinerIntegrationEventService)
        {
            _unitOfWork = unitOfWork;
            _joinerIntegrationEventService = joinerIntegrationEventService;
        }
        public async Task<bool> Handle(UpdateJoinerCommand request, CancellationToken cancellationToken)
        {
            var joiner = await _unitOfWork.JoinerRespository.GetJoinerAsync(request.Id);

            if (joiner == null)
                return false;

            joiner.Name = request.Joiner.Name;
            joiner.Stack = request.Joiner.Stack;
            joiner.DomainExperience = request.Joiner.DomainExperience;
            joiner.Language = request.Joiner.Language;
            joiner.IdentificationNumber = request.Joiner.IdentificationNumber;
            joiner.Role = request.Joiner.Role;

            _unitOfWork.JoinerRespository.Update(joiner);
            var joinerEvent = new JoinerChangedIntegrationEvent(joiner.JoinerId, joiner.IdentificationNumber, joiner.Name, joiner.Stack, joiner.Role, joiner.Language, joiner.DomainExperience);

            _joinerIntegrationEventService.PublishThroughEventBusAsync(joinerEvent);

            _unitOfWork.Commit();

            return true;
        }
    }
}
