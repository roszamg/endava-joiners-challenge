﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Joiner.API.Models;
using Joiner.API.Queries;
using Joiner.Persistence.DataBase.Repositories;
using MediatR;

namespace Joiner.API.Handlers
{
    public class GetJoinerQueryHandler : IRequestHandler<GetJoinerQuery, JoinerDto>
    {   
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public GetJoinerQueryHandler(            
            IMapper mapper, IUnitOfWork unitOfWork)
        {            
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<JoinerDto> Handle(GetJoinerQuery request, CancellationToken cancellationToken)
        {
            var joiner = await _unitOfWork.JoinerRespository.GetJoinerAsync(request.JoinerId);         

            return joiner == null ? null : _mapper.Map<JoinerDto>(joiner);
        }
    }
}
