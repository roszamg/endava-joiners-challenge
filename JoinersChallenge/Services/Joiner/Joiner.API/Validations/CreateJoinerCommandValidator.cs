﻿using FluentValidation;
using Joiner.API.Command;
using Joiner.Persistence.DataBase.Repositories;

namespace Joiner.API.Validations
{
    public class CreateJoinerCommandValidator : AbstractValidator<CreateJoinerCommand>
    {
        private readonly IJoinerRespository _joinerRespository;
        public CreateJoinerCommandValidator(IJoinerRespository joinerRespository)
        {
            _joinerRespository = joinerRespository;

            RuleFor(x => x.Joiner.IdentificationNumber).NotEmpty();
            RuleFor(x => x.Joiner.Name).NotEmpty();
            RuleFor(x => x.Joiner.IdentificationNumber).Must(IsUniqueIdentifier).WithMessage("Identifier must be unique");
        }

        private bool IsUniqueIdentifier(int IdentificationNumber)
        {
            var joiner = _joinerRespository.FindByIdentifacationNumber(IdentificationNumber);
            joiner.Wait();
            
            return joiner.Result == null;
        }
    }
}
