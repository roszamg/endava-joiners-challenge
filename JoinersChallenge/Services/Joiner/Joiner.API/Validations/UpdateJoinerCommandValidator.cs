﻿using FluentValidation;
using Joiner.API.Command;
using Joiner.Persistence.DataBase.Repositories;

namespace Joiner.API.Validations
{
    public class UpdateJoinerCommandValidator : AbstractValidator<UpdateJoinerCommand>
    {
        private readonly IJoinerRespository _joinerRespository;
        public UpdateJoinerCommandValidator(IJoinerRespository joinerRespository)
        {
            _joinerRespository = joinerRespository;

            RuleFor(x => x.Joiner.IdentificationNumber).NotEmpty();
            RuleFor(x => x.Joiner.IdentificationNumber).Must((o, identifier) => { return IsUniqueIdentifier(o.Id, identifier); }).WithMessage("Identifier must be unique");
            RuleFor(x => x.Joiner.Name).NotEmpty();
        }

        private bool IsUniqueIdentifier(int id, int IdentificationNumber)
        {
            var joiner = _joinerRespository.FindByIdentifacationNumber(IdentificationNumber);
            joiner.Wait();

            return joiner.Result == null || joiner.Result.JoinerId == id;
        }
    }
}
