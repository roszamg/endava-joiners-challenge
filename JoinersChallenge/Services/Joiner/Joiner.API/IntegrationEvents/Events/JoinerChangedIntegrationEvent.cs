﻿using EventBusRabbitMQ.Events;

namespace Joiner.API.IntegrationEvents.Events
{
    public class JoinerChangedIntegrationEvent : IntegrationEvent
    {
        public int JoinerId { get; set; }
        public int IdentificationNumber { get; set; }
        public string Name { get; set; }
        public string Stack { get; set; }
        public string Role { get; set; }
        public string Language { get; set; }
        public string DomainExperience { get; set; }

        public JoinerChangedIntegrationEvent(int joinerId,
            int identificationNumber,
            string name,
            string stack,
            string role,
            string englishLevel,
            string domainExperience)
        {
            JoinerId = joinerId;
            IdentificationNumber = identificationNumber;
            Name = name;
            Stack = stack;
            Role = role;
            Language = englishLevel;
            DomainExperience = domainExperience;
        }
    }
}
