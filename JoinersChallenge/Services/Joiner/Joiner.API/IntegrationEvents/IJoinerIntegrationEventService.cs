﻿using EventBusRabbitMQ.Events;

namespace Joiner.API.IntegrationEvents
{
    public interface IJoinerIntegrationEventService
    {
        void PublishThroughEventBusAsync(IntegrationEvent evt);
    }
}
