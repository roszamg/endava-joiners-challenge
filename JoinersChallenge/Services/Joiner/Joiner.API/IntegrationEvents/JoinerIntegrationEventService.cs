﻿using System;
using EventBusRabbitMQ.Bus;
using EventBusRabbitMQ.Events;
using Microsoft.Extensions.Logging;

namespace Joiner.API.IntegrationEvents
{
    public class JoinerIntegrationEventService : IJoinerIntegrationEventService
    {
        private readonly IEventBus _eventBus;
        private readonly ILogger<JoinerIntegrationEventService> _logger;
        public JoinerIntegrationEventService(IEventBus eventBus, ILogger<JoinerIntegrationEventService> logger)
        {
            _eventBus = eventBus;
            _logger = logger;
        }

        public void PublishThroughEventBusAsync(IntegrationEvent evt)
        {
            try
            {
                _logger.LogInformation("----- Publishing integration event: {IntegrationEventId_published}-({@IntegrationEvent})", evt.Id, evt);
                _eventBus.Publish(evt);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ERROR Publishing integration event: {IntegrationEventId} - ({@IntegrationEvent})", evt.Id, evt);

            }
        }
    }
}
