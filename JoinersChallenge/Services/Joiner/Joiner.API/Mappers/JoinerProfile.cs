﻿using AutoMapper;
using Joiner.API.Models;

namespace Joiner.API.Mappers
{
    public class JoinerProfile : Profile
    {
        public JoinerProfile()
        {
            CreateMap<Domain.Joiner, JoinerDto>();         
        }
    }
}
