﻿using Joiner.API.Models;
using MediatR;

namespace Joiner.API.Queries
{
    public class GetJoinerQuery : IRequest<JoinerDto>
    {
        public int JoinerId { get; set; }
        public GetJoinerQuery(int joinerId)
        {
            JoinerId = joinerId;
        }

    }
}
