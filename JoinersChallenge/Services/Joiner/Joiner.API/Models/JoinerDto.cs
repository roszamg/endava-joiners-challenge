﻿namespace Joiner.API.Models
{
    public class JoinerDto
    {
        public int JoinerId { get; set; }
        public int IdentificationNumber { get; set; }
        public string Name { get; set; }
        public string Stack { get; set; }
        public string Role { get; set; }
        public string Language { get; set; }
        public string DomainExperience { get; set; }
    }
}
