﻿namespace Joiner.API.Models
{
    internal class ErrorModelViewModel
    { 
        public string Message { get; set; }

        public string PropertyName { get; set; }
    }
}
