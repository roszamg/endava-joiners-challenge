﻿using Joiner.API.Models;
using MediatR;

namespace Joiner.API.Command
{
    public class UpdateJoinerCommand : IRequest<bool>
    {
        public JoinerDto Joiner { get; set; }

        public int Id { get; set; }

        public UpdateJoinerCommand(JoinerDto joiner, int id)
        {
            Joiner = joiner;
            Id = id;
        }
    }
}
