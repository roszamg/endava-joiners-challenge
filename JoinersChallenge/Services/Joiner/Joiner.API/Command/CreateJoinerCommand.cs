﻿using Joiner.API.Models;
using MediatR;

namespace Joiner.API.Command
{
    public class CreateJoinerCommand : IRequest<JoinerDto>
    {
        public JoinerDto Joiner { get; set; }

        public CreateJoinerCommand(JoinerDto joinerDto)
        {
            Joiner = joinerDto;
        }
    }
}
