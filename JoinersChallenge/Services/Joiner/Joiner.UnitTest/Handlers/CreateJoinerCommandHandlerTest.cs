﻿using System.Threading.Tasks;
using AutoMapper;
using EventBusRabbitMQ.Events;
using Joiner.API.Command;
using Joiner.API.Handlers;
using Joiner.API.IntegrationEvents;
using Joiner.API.Mappers;
using Joiner.API.Models;
using Joiner.Persistence.DataBase.Repositories;
using Moq;
using NUnit.Framework;

namespace Joiner.UnitTest.Handlers
{
    [TestFixture]
    public class CreateJoinerCommandHandlerTest
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private IMapper _mapper;
        private Mock<IJoinerIntegrationEventService> _joinerIntegrationEventService;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _unitOfWork.Setup(c => c.JoinerRespository.Add(It.IsAny<Domain.Joiner>())).Returns(GetJoinerFake());

            var myProfile = new JoinerProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);

            _joinerIntegrationEventService = new Mock<IJoinerIntegrationEventService>();
            _joinerIntegrationEventService.Setup(c => c.PublishThroughEventBusAsync(It.IsAny<IntegrationEvent>()));
        }

        [Test]
        public async Task Handler_Success()
        {
            var handler = new CreateJoinerCommandHandler(_unitOfWork.Object, _mapper, _joinerIntegrationEventService.Object);
            var result = await handler.Handle(new CreateJoinerCommand(GetJoinerDtoFake()), new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.AreEqual(1, result.JoinerId);
        }

        private Domain.Joiner GetJoinerFake()
        {
            return new Domain.Joiner()
            {
                JoinerId = 1,
                DomainExperience = "Domain Experience Test",
                IdentificationNumber = 1111111111,
                Language = "English",
                Name = "Test",
                Role = "Test",
                Stack = "Test"
            };
        }

        private JoinerDto GetJoinerDtoFake()
        {
            return new JoinerDto()
            {
                JoinerId = 1,
                DomainExperience = "Domain Experience Test",
                IdentificationNumber = 1111111111,
                Language = "English",
                Name = "Test",
                Role = "Test",
                Stack = "Test"
            };
        }
    }
}


