﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Joiner.API.Handlers;
using Joiner.API.Mappers;
using Joiner.API.Models;
using Joiner.API.Queries;
using Joiner.Persistence.DataBase.Repositories;
using Moq;
using NUnit.Framework;

namespace Joiner.UnitTest.Handlers
{
    [TestFixture]
    public class GetJoinerQueryHandlerTest
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private IMapper _mapper;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _unitOfWork.Setup(c => c.JoinerRespository.GetJoinerAsync(1).Result).Returns(GetJoinerFake());

            var myProfile = new JoinerProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
        }

        [Test]
        public async Task Handler_Success_Model()
        {
            var handler = new GetJoinerQueryHandler(_mapper, _unitOfWork.Object);
            var result = await handler.Handle(new GetJoinerQuery(1), new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.AreEqual(1, result.JoinerId);
        }

        private Domain.Joiner GetJoinerFake()
        {
            return new Domain.Joiner()
            {
                JoinerId = 1,
                DomainExperience = "Domain Experience Test",
                IdentificationNumber = 1111111111,
                Language = "English",
                Name = "Test",
                Role = "Test",
                Stack = "Test"
            };
        }

        private JoinerDto GetJoinerDtoFake()
        {
            return new JoinerDto()
            {
                JoinerId = 1,
                DomainExperience = "Domain Experience Test",
                IdentificationNumber = 1111111111,
                Language = "English",
                Name = "Test",
                Role = "Test",
                Stack = "Test"
            };
        }

    }
}
