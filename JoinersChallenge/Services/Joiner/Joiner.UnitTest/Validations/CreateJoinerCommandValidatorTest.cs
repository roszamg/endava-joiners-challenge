﻿using FluentValidation.TestHelper;
using Joiner.API.Command;
using Joiner.API.Models;
using Joiner.API.Validations;
using Joiner.Persistence.DataBase.Repositories;
using Moq;
using NUnit.Framework;

namespace Joiner.UnitTest.Validations
{
    [TestFixture]
    public class CreateJoinerCommandValidatorTest
    {
        private CreateJoinerCommandValidator _validator;
        private Mock<IJoinerRespository> _joinerRepository;

        [SetUp]
        public void SetUp()
        {
            _joinerRepository = new Mock<IJoinerRespository>();
            _joinerRepository.Setup(x => x.FindByIdentifacationNumber(111111111).Result).Returns(GetJoinerTest());
            _validator = new CreateJoinerCommandValidator(_joinerRepository.Object);
        }

        [Test]
        public void Should_have_error_when_identifier_not_unique()
        {
            var model = new CreateJoinerCommand(new JoinerDto() { IdentificationNumber = 111111111 });
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(c => c.Joiner.IdentificationNumber);
        }

        [Test]
        public void Should_not_have_error_when_identifier_not_unique()
        {
            var model = new CreateJoinerCommand(new JoinerDto() { IdentificationNumber = 111111112 });
            var result = _validator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(c => c.Joiner.IdentificationNumber);
        }

        [Test]
        public void Should_have_error_when_identifier_is_null()
        {
            var model = new CreateJoinerCommand(new JoinerDto());
            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(c => c.Joiner.IdentificationNumber);
        }

        [Test]
        public void Should_not_have_error_when_identifier_is_specified()
        {
            var model = new CreateJoinerCommand(new JoinerDto() { IdentificationNumber = 1111111112 });
            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(c => c.Joiner.IdentificationNumber);
        }

        [Test]
        public void Should_have_error_when_name_is_null()
        {
            var model = new CreateJoinerCommand(new JoinerDto());
            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(c => c.Joiner.Name);
        }

        [Test]
        public void Should_not_have_error_when_name_is_specified()
        {
            var model = new CreateJoinerCommand(new JoinerDto() { Name = "Test Name" });
            var result = _validator.TestValidate(model);

            result.ShouldNotHaveValidationErrorFor(c => c.Joiner.Name);
        }

        private Domain.Joiner GetJoinerTest()
        {

            return new Domain.Joiner()
            {
                DomainExperience = "Test",
                IdentificationNumber = 111111111,
                JoinerId = 1,
                Language = "Test",
                Name = "Test",
                Role = "Test",
                Stack = "Test"
            };


        }
    }
}
