﻿using System.Threading.Tasks;
using Joiner.API.Command;
using Joiner.API.Controllers;
using Joiner.API.Models;
using Joiner.API.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace Joiner.UnitTest.Controller
{
    [TestFixture]
    public class JoinerControllerTest
    {
        private Mock<IMediator> _mediator;
        private Mock<ILogger<JoinerController>> _logger;

        [SetUp]
        public void SetUp()
        {
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger<JoinerController>>();
        }

        [TestCase(1)]
        public async Task Get_Joiner_Success(int joinerId)
        {
            _mediator.Setup(x => x.Send(It.IsAny<GetJoinerQuery>(), new System.Threading.CancellationToken()))
                .ReturnsAsync(GetJoinerFake());

            var joinerController = new JoinerController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.Get(joinerId);

            //Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [TestCase(5)]
        public async Task Get_Joiner_NotFound(int joinerId)
        {
            _mediator.Setup(x => x.Send(new GetJoinerQuery(joinerId), new System.Threading.CancellationToken()))
                    .ReturnsAsync(GetJoinerFake());

            var joinerController = new JoinerController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.Get(joinerId);

            //Assert
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task Create_Joiner_Success()
        {

            _mediator.Setup(x => x.Send(new CreateJoinerCommand(GetJoinerFake()), new System.Threading.CancellationToken()))
                .ReturnsAsync(GetJoinerFake());


            var joinerController = new JoinerController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.CreateJoiner(GetJoinerFake());

            //Assert
            Assert.IsInstanceOf<CreatedAtActionResult>(result);

        }

        [Test]
        public async Task Update_Joiner_Success()
        {
            _mediator.Setup(x => x.Send(It.IsAny<UpdateJoinerCommand>(), new System.Threading.CancellationToken()))
              .ReturnsAsync(true);

            var joinerController = new JoinerController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.Update(1, GetJoinerFake());

            //Assert
            Assert.IsInstanceOf<CreatedAtActionResult>(result);

        }
        private JoinerDto GetJoinerFake()
        {
            return new JoinerDto()
            {
                JoinerId = 1,
                DomainExperience = "Domain Experience Test",
                IdentificationNumber = 1111111111,
                Language = "English",
                Name = "Test",
                Role = "Test",
                Stack = "Test"
            };
        }
    }
}
