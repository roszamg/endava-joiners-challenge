﻿using AutoMapper;

namespace Report.API.Mappers
{
    public class ReportProfile : Profile
    {
        public ReportProfile()
        {
            CreateMap<Task.Client.ReportDto, Models.ReportDto>()                
            .ForMember(x => x.IsTaskComplete, x => x.MapFrom(y => y.TaskEstimated == 0));
        }
    }
}
