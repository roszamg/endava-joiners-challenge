﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Report.API.Mappers
{
    public class ReportUnfinishedTaskProfile : ReportProfile
    {
        public ReportUnfinishedTaskProfile()
        {
            CreateMap<Task.Client.ReportDto, Models.ReportUnfinishedTaskDto>()
                .ForMember(x => x.DaysToEnd, x => x.MapFrom(y => (decimal)y.TaskEstimated / 8));
        }
    }
}
