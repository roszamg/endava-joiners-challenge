﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using Report.API.Models;

namespace Report.API.Helper
{
    public class CsvHelper : ICsvHelper
    {
        public byte[] Create<T>(List<T> data)
        {
            byte[] csvFileBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(memoryStream, Encoding.UTF8))
                {
                    using (var csv = new CsvWriter(writer, new CsvConfiguration(CultureInfo.InvariantCulture)))
                    {

                        csv.WriteHeader(typeof(T));
                        csv.NextRecord();
                        csv.WriteRecords(data);

                    }
                }

                csvFileBytes = memoryStream.ToArray();
            }

            return csvFileBytes;
        }
    }
}
