﻿using System.Collections.Generic;

namespace Report.API.Helper
{
    public interface ICsvHelper
    {
        byte[] Create<T>(List<T> data);
    }
}
