﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Report.API.Models;
using Task.Client;

namespace Report.API.Services
{
    public class TaskService : ITaskService
    {
        private readonly ITaskClient _taskClient;
        private readonly IMapper _mapper;

        public TaskService(ITaskClient taskClient,
            IMapper mapper)
        {
            _taskClient = taskClient;
            _mapper = mapper;
        }

        public async Task<List<ReportByStackDto>> GetJoinerTaskByStack(int top, string stack)
        {
            var result = await _taskClient.GetTaskJoinerByStackAsync(stack);

            var gr = result.GroupBy(c => c.JoinerIdentifier).Select(c => new ReportByStackDto
            {
                JoinerIdentifier = c.Key,
                JoinerName = c.Select(d => d.JoinerName).FirstOrDefault(),
                AmountTaskComplete = c.Where(d => d.TaskEstimated == 0).Count()
            }).Take(top).OrderBy(c => c.AmountTaskComplete).ToList();

            return gr;
        }

        public async Task<List<Models.ReportDto>> GetTaskByJoiner(int? joinerIdentifier)
        {
            var result = await _taskClient.GetTaskByJoinerAsync(joinerIdentifier);

            result = result.OrderBy(c => c.JoinerIdentifier).ToList();

            return _mapper.Map<List<Models.ReportDto>>(result);
        }

        public async Task<List<ReportUnfinishedTaskDto>> GetUnfinishedTaskByJoiner(int? joinerIdentifier)
        {
            var result = await _taskClient.GetUnfinishedTaskByJoinerAsync(joinerIdentifier);

            return _mapper.Map<List<ReportUnfinishedTaskDto>>(result);
        }
    }
}
