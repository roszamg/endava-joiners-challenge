﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Report.API.Models;

namespace Report.API.Services
{
    public interface ITaskService
    {
        Task<List<ReportByStackDto>> GetJoinerTaskByStack(int top, string stack);
        Task<List<ReportDto>> GetTaskByJoiner(int? joinerIdentifier);
        Task<List<ReportUnfinishedTaskDto>> GetUnfinishedTaskByJoiner(int? joinerIdentifier);
    }
}
