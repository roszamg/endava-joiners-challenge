﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper.Configuration.Attributes;

namespace Report.API.Models
{
    public class ReportUnfinishedTaskDto
    {
        [Name(Constants.CsvHeaders.JoinerIdentifier)]
        public int JoinerIdentifier { get; set; }
        [Name(Constants.CsvHeaders.JoinerName)]
        public string JoinerName { get; set; }
        [Name(Constants.CsvHeaders.Stack)]
        public string Stack { get; set; }
        [Name(Constants.CsvHeaders.TaskName)]
        public string TaskName { get; set; }
        [Name(Constants.CsvHeaders.Estimated)]
        public int TaskEstimated { get; set; }
        [Name(Constants.CsvHeaders.DaysToComplete)]
        public decimal DaysToEnd { get; set; }
    }
}
