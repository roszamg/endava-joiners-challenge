﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper.Configuration.Attributes;

namespace Report.API.Models
{
    public class ReportByStackDto
    {
        [Name(Constants.CsvHeaders.JoinerIdentifier)]
        public int JoinerIdentifier { get; set; }

        [Name(Constants.CsvHeaders.JoinerName)]
        public string JoinerName { get; set; }

        [Name(Constants.CsvHeaders.DaysToComplete)]
        public int AmountTaskComplete { get; set; }
    }
}
