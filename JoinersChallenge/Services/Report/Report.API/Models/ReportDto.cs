﻿using CsvHelper.Configuration.Attributes;

namespace Report.API.Models
{
    public class ReportDto
    {
        [Name(Constants.CsvHeaders.JoinerIdentifier)]
        public int JoinerIdentifier { get; set; }

        [Name(Constants.CsvHeaders.JoinerName)]
        public string JoinerName { get; set; }

        [Name(Constants.CsvHeaders.TaskName)]
        public string TaskName { get; set; }

        [Name(Constants.CsvHeaders.TaskDescription)]
        public string TaskDescription { get; set; }

        [Name(Constants.CsvHeaders.IsTaskComplete)]
        public bool IsTaskComplete { get; set; }
    }
}
