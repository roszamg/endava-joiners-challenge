﻿namespace Report.API.Models
{
    public static class Constants
    {
        public static class CsvHeaders
        {
            public const string JoinerIdentifier = "Joiner Identifier Number";
            public const string JoinerName = "Joiner Name";
            public const string Stack = "Stack";
            public const string TaskName = "Task Name";
            public const string TaskDescription = "Task Description";
            public const string Estimated = "Estimated";
            public const string IsTaskComplete = "Task Complete";
            public const string DaysToComplete = "Days to complete";
        }
    }
}
