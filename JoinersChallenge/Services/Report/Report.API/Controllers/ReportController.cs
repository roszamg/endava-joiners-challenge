﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Report.API.Helper;
using Report.API.Services;

namespace Report.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private const string FileName = "JoinerReport.csv";
        private const string FileType = "text/csv";
        private readonly ITaskService _taskService;
        private readonly ICsvHelper _csvHelper;
        private readonly ILogger<ReportController> _logger;

        public ReportController(ICsvHelper csvHelper,
            ITaskService taskService, ILogger<ReportController> logger)
        {
            _csvHelper = csvHelper;
            _taskService = taskService;
            _logger = logger;
        }

        [HttpGet("[action]/{top}/{stack}")]
        public async Task<FileResult> GetJoinerTaskByStack(int top, string stack)
        {

            _logger.LogInformation("Get Joiner Task By Stack Report");
            var result = await _taskService.GetJoinerTaskByStack(top, stack);

            byte[] csvFileBytes = _csvHelper.Create(result);

            return File(csvFileBytes, FileType, FileName);
        }

        [HttpGet("[action]/joinerIdentifier")]
        public async Task<FileResult> GetTaskByJoiner(int? joinerIdentifier)
        {
            _logger.LogInformation("Get Task By Joiner Report");

            var result = await _taskService.GetTaskByJoiner(joinerIdentifier);

            byte[] csvFileBytes = _csvHelper.Create(result);

            return File(csvFileBytes, FileType, FileName);
        }

        [HttpGet("[Action]/joinerIdentifier")]
        public async Task<FileResult> GetUnfinishedTasks(int? joinerIdentifier)
        {
            _logger.LogInformation("Get Unfinished Tasks Report");

            var result = await _taskService.GetUnfinishedTaskByJoiner(joinerIdentifier);

            byte[] csvFileBytes = _csvHelper.Create(result);

            return File(csvFileBytes, FileType, FileName);
        }
    }
}
