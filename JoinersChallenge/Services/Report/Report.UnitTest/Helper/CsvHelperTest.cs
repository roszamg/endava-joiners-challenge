﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Report.API.Helper;
using Report.API.Models;

namespace Report.UnitTest.Helper
{
    [TestFixture]
    public class CsvHelperTest
    {
        private ICsvHelper _csvHelper;

        [Test]
        public void Create_Success()
        {
            _csvHelper = new Report.API.Helper.CsvHelper();
            var result = _csvHelper.Create(GetReportDtosFake());

            Assert.NotNull(result);            
        }

        private List<ReportDto> GetReportDtosFake()
        {
            return new List<ReportDto>() {
                new ReportDto() {
                JoinerIdentifier = 1111111111,
                JoinerName= "Test Name",
                TaskName="Taks Name Test",
                IsTaskComplete = true,
                TaskDescription = "Description Test"}
            };
        }
    }
}
