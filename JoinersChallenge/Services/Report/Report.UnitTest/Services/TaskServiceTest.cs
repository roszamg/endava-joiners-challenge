﻿using System.Collections.Generic;
using AutoMapper;
using Moq;
using NUnit.Framework;
using Report.API.Mappers;
using Report.API.Services;
using Task.Client;

namespace Report.UnitTest.Services
{
    [TestFixture]
    public class TaskServiceTest
    {
        private Mock<ITaskClient> _taskClient;
        private IMapper _mapper;
        private ITaskService _taskService;

        [SetUp]
        public void SetUp()
        {
            _taskClient = new Mock<ITaskClient>();

        }

        [Test]
        public async System.Threading.Tasks.Task GetJoinerTaskByStack_Success()
        {
            _taskClient.Setup(c => c.GetTaskJoinerByStackAsync(It.IsAny<string>()).Result).Returns(GetReportDtosFake());
            _taskService = new TaskService(_taskClient.Object, _mapper);
            var result = await _taskService.GetJoinerTaskByStack(1, ".net");

            Assert.IsNotNull(result);
            Assert.AreEqual(1111111111, result[0].JoinerIdentifier);
        }
    
        [Test]
        public async System.Threading.Tasks.Task GetTaskByJoiner_Success()
        {
            var myProfile = new ReportProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);

            _taskClient.Setup(c => c.GetTaskByJoinerAsync(It.IsAny<int?>()).Result).Returns(GetReportDtosFake());
            _taskService = new TaskService(_taskClient.Object, _mapper);    

            var result = await _taskService.GetTaskByJoiner(1111111111);

            Assert.IsNotNull(result);
            Assert.AreEqual(1111111111, result[0].JoinerIdentifier);

        }

        [Test]
        public async System.Threading.Tasks.Task GetUnfinishedTaskByJoiner_Success()
        {
            var myProfile = new ReportUnfinishedTaskProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);

            _taskClient.Setup(c => c.GetUnfinishedTaskByJoinerAsync(It.IsAny<int?>()).Result).Returns(GetReportDtosFake());
            _taskService = new TaskService(_taskClient.Object, _mapper);

            var result = await _taskService.GetUnfinishedTaskByJoiner(1111111111);

            Assert.IsNotNull(result);
            Assert.AreEqual(1111111111, result[0].JoinerIdentifier);

        }

        private ICollection<ReportDto> GetReportDtosFake()
        {
            return new List<ReportDto>() {
                new ReportDto() {
                JoinerIdentifier = 1111111111,
                JoinerName= "Test Name",
                Stack = ".net",
                TaskDescription="Task Description Test",
                TaskEstimated=5,
                TaskName="Taks Name Test"}
            };
        }
    }
}
