﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Report.API.Controllers;
using Report.API.Helper;
using Report.API.Models;
using Report.API.Services;

namespace Report.UnitTest.Controllers
{
    [TestFixture]
    public class ReportControllerTest
    {
        private Mock<ITaskService> _taskService;
        private Mock<ICsvHelper> _csvHelper;
        private Mock<ILogger<ReportController>> _logger;


        [SetUp]
        public void SetUp()
        {
            _taskService = new Mock<ITaskService>();
            _csvHelper = new Mock<ICsvHelper>();
            _logger = new Mock<ILogger<ReportController>>();
        }

        [Test]
        public async System.Threading.Tasks.Task GetJoinerTaskByStack_Success()
        {

            var csvHelper = new Report.API.Helper.CsvHelper();
            var resultCsv = csvHelper.Create(GetReportByStackDtoFake());

            _taskService.Setup(c => c.GetJoinerTaskByStack(It.IsAny<int>(), It.IsAny<string>()).Result).Returns(GetReportByStackDtoFake());

            var reportController = new ReportController(csvHelper, _taskService.Object, _logger.Object);
            var result = await reportController.GetJoinerTaskByStack(5, ".net");

            Assert.AreEqual("JoinerReport.csv", result.FileDownloadName);
            Assert.AreEqual("text/csv", result.ContentType);
        }

        [Test]
        public async System.Threading.Tasks.Task GetTaskByJoiner_Success()
        {

            var csvHelper = new Report.API.Helper.CsvHelper();
            var resultCsv = csvHelper.Create(GetReportDtosFake());

            _taskService.Setup(c => c.GetTaskByJoiner(It.IsAny<int>()).Result).Returns(GetReportDtosFake());

            var reportController = new ReportController(csvHelper, _taskService.Object, _logger.Object);
            var result = await reportController.GetTaskByJoiner(1111111111);

            Assert.AreEqual("JoinerReport.csv", result.FileDownloadName);
            Assert.AreEqual("text/csv", result.ContentType);
        }

        [Test]
        public async System.Threading.Tasks.Task GetUnfinishedTasks_Success()
        {

            var csvHelper = new Report.API.Helper.CsvHelper();
            var resultCsv = csvHelper.Create(GetListReportUnfinishedTaskDtoFake());

            _taskService.Setup(c => c.GetUnfinishedTaskByJoiner(It.IsAny<int>()).Result).Returns(GetListReportUnfinishedTaskDtoFake());

            var reportController = new ReportController(csvHelper, _taskService.Object, _logger.Object);
            var result = await reportController.GetUnfinishedTasks(1111111111);

            Assert.AreEqual("JoinerReport.csv", result.FileDownloadName);
            Assert.AreEqual("text/csv", result.ContentType);

        }
        private List<ReportByStackDto> GetReportByStackDtoFake()
        {
            return new List<ReportByStackDto>() { new ReportByStackDto() {
             AmountTaskComplete=5, JoinerIdentifier = 111111111, JoinerName="Test Name"
            } };
        }

        public List<ReportUnfinishedTaskDto> GetListReportUnfinishedTaskDtoFake()
        {
            return new List<ReportUnfinishedTaskDto>()
            {
                new ReportUnfinishedTaskDto() {
                    DaysToEnd =2,
                    JoinerIdentifier=1111111111,
                    JoinerName="Joiner Test",
                    Stack=".net",
                    TaskEstimated=8,
                    TaskName="Test Task"
                }
            };
        }

        private List<ReportDto> GetReportDtosFake()
        {
            return new List<ReportDto>() {
                new ReportDto() {
                JoinerIdentifier = 1111111111,
                JoinerName= "Test Name",
                TaskName="Taks Name Test",
                IsTaskComplete = true,
                TaskDescription = "Description Test"}
            };
        }

    }
}
