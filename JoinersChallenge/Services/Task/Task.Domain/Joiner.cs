﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Task.Domain
{
    public class Joiner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        public int JoinerId { get; set; }
        public int IdentificationNumber { get; set; }
        public string Name { get; set; }
        public string Stack { get; set; }
        public string Role { get; set; }
        public string Language { get; set; }
        public string DomainExperience { get; set; }        
    }
}
