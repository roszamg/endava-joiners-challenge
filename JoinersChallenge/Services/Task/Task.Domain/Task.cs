﻿namespace Task.Domain
{
    public class Task
    {
        public int? ParentTaskId { get; set; }
        public int TaskId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int EstimatedRequiereHours { get; set; }
        public string Stack { get; set; }
        public int JoinerId { get; set; }
        public Joiner Joiner { get; set; }
    }
}
