﻿using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Task.API.Command;
using Task.API.Controllers;
using Task.API.Models;
using Task.API.Queries;

namespace Task.UnitTests.Controllers
{
    [TestFixture]
    public class TaskControllerTest
    {
        private Mock<IMediator> _mediator;
        private Mock<ILogger<TaskController>> _logger;

        [SetUp]
        public void SetUp()
        {
            _mediator = new Mock<IMediator>();
            _logger = new Mock<ILogger<TaskController>>();
        }

        [TestCase(1)]
        public async System.Threading.Tasks.Task Get_Task_Success(int taskId)
        {
            _mediator.Setup(x => x.Send(It.IsAny<GetTaskQuery>(), new System.Threading.CancellationToken()))
                .ReturnsAsync(GetTaskFake());

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.Get(taskId);

            //Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [TestCase(5)]
        public async System.Threading.Tasks.Task Get_Task_NotFound(int joinerId)
        {
            _mediator.Setup(x => x.Send(new GetTaskQuery(joinerId), new System.Threading.CancellationToken()))
                    .ReturnsAsync(GetTaskFake());

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.Get(joinerId);

            //Assert
            Assert.IsInstanceOf<NotFoundResult>(result);
        }


        [Test]
        public async System.Threading.Tasks.Task Get_AllTask_Success()
        {
            _mediator.Setup(x => x.Send(It.IsAny<GetAllTaskQuery>(), new System.Threading.CancellationToken()))
                .ReturnsAsync(GetAllTaskFake());

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.GetAllTask();

            //Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async System.Threading.Tasks.Task Get_AllTask_NotFound()
        {
            _mediator.Setup(x => x.Send(new GetAllTaskQuery(), new System.Threading.CancellationToken()));

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.GetAllTask();

            //Assert
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [TestCase(".net")]
        public async System.Threading.Tasks.Task GetJoinerTaskByStackQuery_Success(string stack)
        {
            _mediator.Setup(x => x.Send(It.IsAny<GetJoinerByStackQuery>(), new System.Threading.CancellationToken()))
                .ReturnsAsync(GetReportDtoFake());

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.GetTaskJoinerByStack(stack);

            //Assert
            Assert.IsInstanceOf<List<ReportDto>>(result);
        }

        public async System.Threading.Tasks.Task GetTaskByJoiner_joiner_is_null_Success()
        {

            _mediator.Setup(x => x.Send(It.IsAny<GetTaskByJoinerQuery>(), new System.Threading.CancellationToken()))
                   .ReturnsAsync(GetReportDtoFake());

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.GetTaskByJoiner();

            //Assert
            Assert.IsInstanceOf<List<ReportDto>>(result);
        }

        public async System.Threading.Tasks.Task GetTaskByJoiner_joiner_has_value_Success()
        {

            _mediator.Setup(x => x.Send(It.IsAny<GetTaskByJoinerQuery>(), new System.Threading.CancellationToken()))
                   .ReturnsAsync(GetReportDtoFake());

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.GetTaskByJoiner(1);

            //Assert
            Assert.IsInstanceOf<List<ReportDto>>(result);
        }

        [Test]
        public async System.Threading.Tasks.Task GetUnfinishedTask_Success()
        {

            _mediator.Setup(x => x.Send(It.IsAny<GetUnfinishedTaskQuery>(), new System.Threading.CancellationToken()))
                   .ReturnsAsync(GetReportDtoFake());

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.GetUnfinishedTaskByJoiner();

            //Assert
            Assert.IsInstanceOf<List<ReportDto>>(result);
        }

        [TestCase(1)]
        public async System.Threading.Tasks.Task GetUnfinishedTaskByJoiner_Success(int joinerId)
        {

            _mediator.Setup(x => x.Send(It.IsAny<GetUnfinishedTaskQuery>(), new System.Threading.CancellationToken()))
                   .ReturnsAsync(GetReportDtoFake());

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.GetUnfinishedTaskByJoiner(1);

            //Assert
            Assert.IsInstanceOf<List<ReportDto>>(result);
        }


        [Test]
        public async System.Threading.Tasks.Task Create_Task_Success()
        {

            _mediator.Setup(x => x.Send(new CreateTaskCommand(GetTaskFake()), new System.Threading.CancellationToken()))
                .ReturnsAsync(GetTaskFake());


            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.Post(GetTaskFake());

            //Assert
            Assert.IsInstanceOf<CreatedAtActionResult>(result);

        }

        [Test]
        public async System.Threading.Tasks.Task Update_Task_Success()
        {
            _mediator.Setup(x => x.Send(It.IsAny<UpdateTaskCommand>(), new System.Threading.CancellationToken()))
              .ReturnsAsync(true);

            var joinerController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await joinerController.Put(1, GetTaskFake());

            //Assert
            Assert.IsInstanceOf<CreatedAtActionResult>(result);

        }

        [TestCase(1)]
        public async System.Threading.Tasks.Task Delete_Task_Success(int taskId)
        {
            _mediator.Setup(x => x.Send(It.IsAny<DeleteTaskCommand>(), new System.Threading.CancellationToken()))
              .ReturnsAsync(true);

            var taskController = new TaskController(_mediator.Object, _logger.Object);

            //Action
            var result = await taskController.Delete(taskId);

            //Assert
            Assert.IsInstanceOf<OkResult>(result);

        }

        private TaskDto GetTaskFake()
        {
            return new TaskDto()
            {
                Description = "Description Task",
                EstimatedRequiereHours = 5,
                JoinerId = 1,
                Name = "Test task",
                ParentTaskId = null,
                Stack = "Stack test",
                TaskId = 1
            };
        }

        private List<TaskDto> GetAllTaskFake()
        {
            return new List<TaskDto>(){new TaskDto()
            {
                Description = "Description Task",
                EstimatedRequiereHours = 5,
                JoinerId = 1,
                Name = "Test task",
                ParentTaskId = null,
                Stack = "Stack test",
                TaskId = 1
            } };
        }

        private List<ReportDto> GetReportDtoFake()
        {
            return new List<ReportDto>() {
                new ReportDto() {
             JoinerName="Test Name",
              Stack="Stack test",
               TaskDescription="Task Description",
                TaskEstimated=5,
                 TaskName="Task Name"
            }
            };
        }
    }
}
