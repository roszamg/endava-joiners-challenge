﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using AutoMapper;
using Joiner.API.Mappers;
using Moq;
using NUnit.Framework;
using Task.API.Handlers;
using Task.API.Queries;
using Task.Persistence.DataBase.Repositories;

namespace Task.UnitTests.Handlers
{
    [TestFixture]
    public class GetTaskByJoinerQueryHandlerTest
    {
        private Mock<ITaskRepository> _taskRepository;
        private IMapper _mapper;

        [SetUp]
        public void SetUp()
        {
            _taskRepository = new Mock<ITaskRepository>();
            _taskRepository.Setup(c => c.GetAsync(It.IsAny<Expression<Func<Domain.Task, bool>>>()).Result).Returns(GetTaskFake());

            var myProfile = new TaskProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
        }

        [Test]
        public async System.Threading.Tasks.Task GetTaskByJoinerQuery_Handler_Joiner_is_null_Success()
        {
            var handler = new GetTaskByJoinerQueryHandler(_mapper, _taskRepository.Object);
            var result = await handler.Handle(new GetTaskByJoinerQuery(), new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.AreEqual(111111111, result[0].JoinerIdentifier);
        }

        [Test]
        public async System.Threading.Tasks.Task GetTaskByJoinerQuery_Handler_Joiner_has_value_Success()
        {
            var handler = new GetTaskByJoinerQueryHandler(_mapper, _taskRepository.Object);
            var result = await handler.Handle(new GetTaskByJoinerQuery(111111111), new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.AreEqual(111111111, result[0].JoinerIdentifier);
        }

        private List<Domain.Task> GetTaskFake()
        {
            return new List<Domain.Task>(){  new Domain.Task()
            {
                Description = "Description Test",
                EstimatedRequiereHours = 5,
                Name = "Test Name",
                ParentTaskId = null,
                Stack = "Stack Test",
                TaskId = 1,
                JoinerId = 1,
                Joiner = new Domain.Joiner()
                {
                    DomainExperience = "Test",
                    IdentificationNumber = 111111111,
                    JoinerId = 1,
                    Language = "Test",
                    Name = "Test",
                    Role = "Test",
                    Stack = "Test .net"
                }
            } };
        }
    }
}
