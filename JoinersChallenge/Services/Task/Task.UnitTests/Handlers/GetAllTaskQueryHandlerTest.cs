﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Joiner.API.Mappers;
using Moq;
using NUnit.Framework;
using Task.API.Handlers;
using Task.API.Queries;
using Task.Persistence.DataBase.Repositories;

namespace Task.UnitTests.Handlers
{
    [TestFixture]
    public class GetAllTaskQueryHandlerTest
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private IMapper _mapper;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _unitOfWork.Setup(c => c.TaskRespository.GetAllTaskAsync().Result).Returns(GetTaskFake());

            var myProfile = new TaskProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
        }

        [Test]
        public async System.Threading.Tasks.Task Get_All_Handler_Success()
        {
            var handler = new GetAllTaskQueryHandler(_unitOfWork.Object, _mapper);
            var result = await handler.Handle(new GetAllTaskQuery(), new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.AreEqual(1, result[0].TaskId);

        }

        private List<Domain.Task> GetTaskFake()
        {
            return new List<Domain.Task>(){new Domain.Task()
            {
                Description = "Description Test",
                EstimatedRequiereHours = 5,
                Name = "Test Name",
                ParentTaskId = null,
                Stack = "Stack Test",
                TaskId = 1,
                JoinerId = 1,
                Joiner = new Domain.Joiner()
                {
                    DomainExperience = "Test",
                    IdentificationNumber = 111111111,
                    JoinerId = 1,
                    Language = "Test",
                    Name = "Test",
                    Role = "Test",
                    Stack = "Test"
                }
            } };
        }
    }
}


