﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using NUnit.Framework;
using Task.API.Command;
using Task.API.Handlers;
using Task.Persistence.DataBase.Repositories;

namespace Task.UnitTests.Handlers
{
    [TestFixture]
    public class DeleteTaskCommandHandlerTest
    {
        private Mock<IUnitOfWork> _unitOfWork;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();            
            _unitOfWork.Setup(c => c.TaskRespository.Delete(It.IsAny<Domain.Task>()));
        }

        [Test]
        public async System.Threading.Tasks.Task Delete_Handler_Task_Success()
        {
            _unitOfWork.Setup(c => c.TaskRespository.GetTaskAsync(It.IsAny<int>()).Result).Returns(GetTaskFake());
            var handler = new DeleteTaskCommandHandler(_unitOfWork.Object);
            var result = await handler.Handle(new DeleteTaskCommand(1), new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.IsTrue(result);
        }

        [Test]
        public async System.Threading.Tasks.Task Delete_Handler_Task_Not_Found()
        {
            _unitOfWork.Setup(c => c.TaskRespository.GetTaskAsync(It.IsAny<int>()).Result);

            var handler = new DeleteTaskCommandHandler(_unitOfWork.Object);
            var result = await handler.Handle(new DeleteTaskCommand(2), new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.IsFalse(result);
        }

        private Domain.Task GetTaskFake()
        {
            return new Domain.Task()
            {
                Description = "Description Test",
                EstimatedRequiereHours = 5,
                Name = "Test Name",
                ParentTaskId = null,
                Stack = "Stack Test",
                TaskId = 1,
                JoinerId = 1,
                Joiner = new Domain.Joiner()
                {
                    DomainExperience = "Test",
                    IdentificationNumber = 111111111,
                    JoinerId = 1,
                    Language = "Test",
                    Name = "Test",
                    Role = "Test",
                    Stack = "Test"
                }
            };
        }
    }
}
