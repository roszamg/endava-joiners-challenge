﻿using Moq;
using NUnit.Framework;
using Task.API.Command;
using Task.API.Handlers;
using Task.API.Models;
using Task.Persistence.DataBase.Repositories;

namespace Task.UnitTests.Handlers
{
    [TestFixture]
    public class UpdateTaskCommandHandlerTest
    {
        private Mock<IUnitOfWork> _unitOfWork;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _unitOfWork.Setup(c => c.TaskRespository.GetTaskAsync(It.IsAny<int>()).Result).Returns(GetTaskFake());
        }

        [Test]
        public async System.Threading.Tasks.Task Update_handler_Success()
        {

            var handler = new UpdateTaskCommandHandler(_unitOfWork.Object);
            var result = await handler.Handle(new UpdateTaskCommand(GetTaskDtoFake(), 1), new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.IsTrue(result);
        }

        private TaskDto GetTaskDtoFake()
        {
            return new TaskDto()
            {
                Description = "Description Task",
                EstimatedRequiereHours = 5,
                JoinerId = 1,
                Name = "Test task",
                ParentTaskId = null,
                Stack = "Stack test",
                TaskId = 1
            };
        }
        private Domain.Task GetTaskFake()
        {
            return new Domain.Task()
            {
                Description = "Description Test",
                EstimatedRequiereHours = 5,
                Name = "Test Name",
                ParentTaskId = null,
                Stack = "Stack Test",
                TaskId = 1,
                JoinerId = 1,
                Joiner = new Domain.Joiner()
                {
                    DomainExperience = "Test",
                    IdentificationNumber = 111111111,
                    JoinerId = 1,
                    Language = "Test",
                    Name = "Test",
                    Role = "Test",
                    Stack = "Test"
                }
            };
        }
    }
}
