﻿using AutoMapper;
using Joiner.API.Mappers;
using Moq;
using NUnit.Framework;
using Task.API.Command;
using Task.API.Handlers;
using Task.API.Models;
using Task.Persistence.DataBase.Repositories;

namespace Task.UnitTests.Handlers
{
    [TestFixture]
    public class CreateTaskCommandHandlerTest
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private IMapper _mapper;        

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _unitOfWork.Setup(c => c.TaskRespository.Add(It.IsAny<Domain.Task>())).Returns(GetTaskFake());

            var myProfile = new TaskProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);            
        }

        [Test]
        public async System.Threading.Tasks.Task Handler_Success()
        {
            var handler = new CreateTaskCommandHandler(_unitOfWork.Object, _mapper);
            var result = await handler.Handle(new CreateTaskCommand(GetTaskDtoFake()), new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.AreEqual(1, result.JoinerId);
        }

        private Domain.Task GetTaskFake()
        {
            return new Domain.Task()
            {
                Description = "Description Test",
                EstimatedRequiereHours = 5,
                Name = "Test Name",
                ParentTaskId = null,
                Stack = "Stack Test",
                TaskId = 1,
                JoinerId = 1,
                Joiner = new Domain.Joiner()
                {
                    DomainExperience = "Test",
                    IdentificationNumber = 111111111,
                    JoinerId = 1,
                    Language = "Test",
                    Name = "Test",
                    Role = "Test",
                    Stack = "Test"
                }
            };
        }

        private TaskDto GetTaskDtoFake()
        {
            return new TaskDto()
            {
                Description = "Description Task",
                EstimatedRequiereHours = 5,
                JoinerId = 1,
                Name = "Test task",
                ParentTaskId = null,
                Stack = "Stack test",
                TaskId = 1
            };
        }
    }
}
