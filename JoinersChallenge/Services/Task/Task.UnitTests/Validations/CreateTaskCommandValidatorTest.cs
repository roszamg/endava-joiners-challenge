﻿

using FluentValidation.TestHelper;
using Moq;
using NUnit.Framework;
using Task.API.Command;
using Task.API.Models;
using Task.API.Validations;
using Task.Persistence.DataBase.Repositories;

namespace Task.UnitTest.Validations
{
    [TestFixture]
    public class CreateTaskCommandValidatorTest
    {
        private CreateTaskCommandValidation _validator;
        private Mock<IUnitOfWork> _unitOfWork;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _validator = new CreateTaskCommandValidation(_unitOfWork.Object);
        }

        [Test]
        public void Should_have_error_when_name_is_empty()
        {
            _unitOfWork.Setup(x => x.TaskRespository.GetTaskAsync(It.IsAny<int>()).Result).Returns(GetTaskTest());
            _unitOfWork.Setup(x => x.JoinerRepository.GetJoinerAsync(It.IsAny<int>()).Result).Returns(GetJoinerTest());

            var model = new CreateTaskCommand(new TaskDto());
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(c => c.Task.Name);
        }

        [Test]
        public void Should_not_have_error_when_name_not_empty()
        {
            _unitOfWork.Setup(x => x.TaskRespository.GetTaskAsync(It.IsAny<int>()).Result).Returns(GetTaskTest());
            _unitOfWork.Setup(x => x.JoinerRepository.GetJoinerAsync(It.IsAny<int>()).Result).Returns(GetJoinerTest());

            var model = new CreateTaskCommand(new TaskDto() { Name = "Test Task" });
            var result = _validator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(c => c.Task.Name);
        }

        [Test]
        public void Should_have_error_when_joiner_not_exists()
        {
            _unitOfWork.Setup(x => x.TaskRespository.GetTaskAsync(It.IsAny<int>()).Result).Returns(GetTaskTest());
            _unitOfWork.Setup(x => x.JoinerRepository.GetJoinerAsync(It.IsAny<int>()).Result);

            var model = new CreateTaskCommand(new TaskDto() { Name = "Test Task", JoinerId = 0 });
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(c => c.Task.JoinerId);
        }

        [Test]
        public void Should_have_error_when_parentId_not_onelevel()
        {
            _unitOfWork.Setup(x => x.TaskRespository.GetTaskAsync(It.IsAny<int>()).Result).Returns(GetTaskParentNotOneLevelTest());
            _unitOfWork.Setup(x => x.JoinerRepository.GetJoinerAsync(It.IsAny<int>()).Result).Returns(GetJoinerTest());

            var model = new CreateTaskCommand(new TaskDto() { Name = "Test Task", ParentTaskId = 2 });
            var result = _validator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(c => c.Task.ParentTaskId);
        }

        private Domain.Joiner GetJoinerTest()
        {
            return new Domain.Joiner()
            {
                DomainExperience = "Test",
                IdentificationNumber = 111111111,
                JoinerId = 1,
                Language = "Test",
                Name = "Test",
                Role = "Test",
                Stack = "Test"
            };
        }
        private Domain.Task GetTaskTest()
        {
            return new Domain.Task()
            {
                Description = "Description Test",
                EstimatedRequiereHours = 5,
                Name = "Test Name",
                ParentTaskId = 1,
                Stack = "Stack Test",
                TaskId = 1,
                JoinerId = 1,
                Joiner = new Domain.Joiner()
                {
                    DomainExperience = "Test",
                    IdentificationNumber = 111111111,
                    JoinerId = 1,
                    Language = "Test",
                    Name = "Test",
                    Role = "Test",
                    Stack = "Test"
                }
            };
        }

        private Domain.Task GetTaskParentNotOneLevelTest()
        {
            return new Domain.Task()
            {
                Description = "Description Test",
                EstimatedRequiereHours = 5,
                Name = "Test Name",
                ParentTaskId = 2,
                Stack = "Stack Test",
                TaskId = 1,
                JoinerId = 1,
                Joiner = new Domain.Joiner()
                {
                    DomainExperience = "Test",
                    IdentificationNumber = 111111111,
                    JoinerId = 1,
                    Language = "Test",
                    Name = "Test",
                    Role = "Test",
                    Stack = "Test"
                }
            };
        }
    }
}
