﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Task.Domain;

namespace Task.Persistence.DataBase.Repositories
{
    [ExcludeFromCodeCoverage]

    public class JoinerRepository : BaseRepository<Joiner> , IJoinerRepository
    {
        public JoinerRepository(TaskContext context) : base(context)
        {

        }

        public async Task<Joiner> GetJoinerAsync(int joinerId)
        {
            var joiner = await _dbSet.FirstOrDefaultAsync(i => i.JoinerId == joinerId);

            return joiner;
        }
    }
}
