﻿using System.Threading.Tasks;

namespace Task.Persistence.DataBase.Repositories
{
    public interface IJoinerRepository : IBaseRepository<Domain.Joiner>
    {
        Task<Domain.Joiner> GetJoinerAsync(int taskId);
    }
}
