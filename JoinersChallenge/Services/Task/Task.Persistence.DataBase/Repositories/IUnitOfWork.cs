﻿namespace Task.Persistence.DataBase.Repositories
{
    public interface IUnitOfWork
    {
        ITaskRepository TaskRespository { get; }
        IJoinerRepository JoinerRepository { get; }

        int Commit();
    }
}
