﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Task.Persistence.DataBase.Repositories
{
    [ExcludeFromCodeCoverage]

    public class TaskRepository : BaseRepository<Domain.Task>, ITaskRepository
    {
        public TaskRepository(TaskContext taskContext) : base(taskContext)
        {

        }

        public async Task<Domain.Task> GetTaskAsync(int taskId)
        {
            var joiner = await _dbSet.FirstOrDefaultAsync(i => i.TaskId == taskId);

            return joiner;
        }

        public async Task<List<Domain.Task>> GetAllTaskAsync()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async Task<List<Domain.Task>> GetAsync(Expression<Func<Domain.Task, bool>> whereCondition = null)
        {
            IQueryable<Domain.Task> query = _dbSet.Include(c => c.Joiner);

            if (whereCondition != null)
            {
                query = query.Where(whereCondition);
            }

            return await query.ToListAsync();
        }
    }
}
