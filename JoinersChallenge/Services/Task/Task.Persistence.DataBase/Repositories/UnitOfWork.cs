﻿using System;

namespace Task.Persistence.DataBase.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly TaskContext _context;

        public ITaskRepository TaskRespository { get; }
        public IJoinerRepository JoinerRepository { get; }
        public UnitOfWork(
            TaskContext context,
            ITaskRepository taskRespository, IJoinerRepository joinerRepository)
        {
            _context = context;
            TaskRespository = taskRespository;
            JoinerRepository = joinerRepository;
        }
        public int Commit()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

    }
}
