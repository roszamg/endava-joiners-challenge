﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Task.Persistence.DataBase.Repositories
{
    public interface ITaskRepository : IBaseRepository<Domain.Task>
    {
        Task<Domain.Task> GetTaskAsync(int taskId);

        Task<List<Domain.Task>> GetAllTaskAsync();

        Task<List<Domain.Task>> GetAsync(Expression<Func<Domain.Task, bool>> whereCondition = null);
    }
}
