﻿namespace Task.Persistence.DataBase.Repositories
{
    public interface IBaseRepository<TEntity>
    {
        TEntity Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
    }
}
