﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace Task.Persistence.DataBase
{
    [ExcludeFromCodeCoverage]
    public class TaskContext : DbContext
    {
        public TaskContext(DbContextOptions<TaskContext> options) : base(options)
        { }

        public DbSet<Domain.Task> Tasks { get; set; }
        public DbSet<Domain.Joiner> Joiners { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Domain.Joiner>(entity =>
            {
                entity.HasIndex(x => x.JoinerId);
                entity.Property(x => x.Name).IsRequired();
            });

            modelBuilder.Entity<Domain.Task>(entity =>
            {
                entity.HasIndex(x => x.TaskId);
                entity.Property(x => x.Name).IsRequired();
                entity.HasOne(x => x.Joiner);
            });
        }

    }
}
