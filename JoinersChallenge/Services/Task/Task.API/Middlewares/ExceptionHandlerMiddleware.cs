﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Text;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Task.API.Models;

namespace Task.API.Middlewares
{
    /// <summary>
    /// Central error/exception handler Middleware
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ExceptionHandlerMiddleware
    {
        private const string JsonContentType = "application/json";
        private readonly RequestDelegate request;
        private readonly ILogger<ExceptionHandlerMiddleware> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlerMiddleware"/> class.
        /// </summary>
        /// <param name="next">The next.</param>
        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<ExceptionHandlerMiddleware> logger)
        {
            this.request = next;
            _logger = logger;
        }

        /// <summary>
        /// Invokes the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public System.Threading.Tasks.Task Invoke(HttpContext context) => this.InvokeAsync(context);

        async System.Threading.Tasks.Task InvokeAsync(HttpContext context)
        {
            try
            {
                await this.request(context);
            }
            catch (Exception exception)
            {

                if (!(exception is ValidationException validationException))
                {
                    _logger.LogError("An exception an occurr: " + exception.Message);

                    throw exception;
                }

                var errors = validationException.Errors.Select(err => new ErrorModelViewModel
                {
                    PropertyName = err.PropertyName,
                    Message = err.ErrorMessage
                });

                var errorText = JsonConvert.SerializeObject(errors);

                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Response.ContentType = JsonContentType;

                _logger.LogError(string.Format("Validation exception. {0}", errors.Select(c => string.Format("{0} {1}", c.PropertyName, c.PropertyName))));

                await context.Response.WriteAsync(errorText, Encoding.UTF8);
            }
        }
    }
}
