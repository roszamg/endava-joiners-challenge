﻿using MediatR;
using Task.API.Models;

namespace Task.API.Command
{
    public class CreateTaskCommand : IRequest<TaskDto>
    {
        public TaskDto Task { get; set; }

        public CreateTaskCommand(TaskDto task)
        {
            Task = task;
        }
    }
}
