﻿using MediatR;
using Task.API.Models;

namespace Task.API.Command
{
    public class UpdateTaskCommand : IRequest<bool>
    {
        public TaskDto Task { get; set; }
        public int TaskId { get; set; }
        public UpdateTaskCommand(TaskDto task, int taskId)
        {
            Task = task;
            TaskId = taskId;
        }
    }
}
