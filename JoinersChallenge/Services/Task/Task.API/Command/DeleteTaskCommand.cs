﻿using MediatR;

namespace Task.API.Command
{
    public class DeleteTaskCommand : IRequest<bool>
    {
        public int TaskId { get; set; }
        public DeleteTaskCommand(int taskId)
        {
            TaskId = taskId;
        }
    }
}
