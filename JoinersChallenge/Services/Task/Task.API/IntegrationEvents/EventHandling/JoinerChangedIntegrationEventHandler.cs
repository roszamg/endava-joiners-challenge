﻿using EventBusRabbitMQ.Bus;
using Task.API.IntegrationEvents.Events;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.IntegrationEvents.EventHandling
{
    public class JoinerChangedIntegrationEventHandler : IIntegrationEventHandler<JoinerChangedIntegrationEvent>
    {        
        private readonly IUnitOfWork _unitOfWork;

        public JoinerChangedIntegrationEventHandler()
        {

        }
        public JoinerChangedIntegrationEventHandler(IUnitOfWork unitOfWork)
        {            
            _unitOfWork = unitOfWork;
        }

        public async System.Threading.Tasks.Task Handle(JoinerChangedIntegrationEvent @event)
        {
            var joiner = await _unitOfWork.JoinerRepository.GetJoinerAsync(@event.JoinerId);

            if (joiner == null)
            {
                joiner = new Domain.Joiner()
                {
                    JoinerId = @event.JoinerId,
                    Name = @event.Name,
                    IdentificationNumber = @event.IdentificationNumber,
                    Stack = @event.Stack,
                    Role = @event.Role,
                    Language = @event.Language,
                    DomainExperience = @event.DomainExperience
                };

                _unitOfWork.JoinerRepository.Add(joiner);
            }
            else
            {
                joiner.Name = @event.Name;
                joiner.Stack = @event.Stack;
                joiner.Role = @event.Role;
                joiner.Language = @event.Language;
                joiner.DomainExperience = @event.DomainExperience;
                _unitOfWork.JoinerRepository.Update(joiner);
            }

            _unitOfWork.Commit();
        }
    }
}
