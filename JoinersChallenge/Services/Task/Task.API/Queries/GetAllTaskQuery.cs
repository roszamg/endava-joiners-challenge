﻿using System.Collections.Generic;
using MediatR;
using Task.API.Models;

namespace Task.API.Queries
{
    public class GetAllTaskQuery : IRequest<List<TaskDto>>
    {
    }
}
