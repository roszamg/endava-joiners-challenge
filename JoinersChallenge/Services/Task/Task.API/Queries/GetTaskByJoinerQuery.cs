﻿using System.Collections.Generic;
using MediatR;
using Task.API.Models;

namespace Task.API.Queries
{
    public class GetTaskByJoinerQuery : IRequest<List<ReportDto>>
    {
        public int? IdentificationNumber { get; set; }

        public GetTaskByJoinerQuery(int? identificationNumber = null)
        {
            IdentificationNumber = identificationNumber;
        }
    }
}
