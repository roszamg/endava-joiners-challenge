﻿using System.Collections.Generic;
using MediatR;
using Task.API.Models;

namespace Task.API.Queries
{
    public class GetUnfinishedTaskQuery : IRequest<List<ReportDto>>
    {
        public int? JoinerIdentifier { get; set; }
        public GetUnfinishedTaskQuery(int? joinerIdentifier)
        {
            JoinerIdentifier = joinerIdentifier;
        }
    }
}
