﻿using MediatR;
using Task.API.Models;

namespace Task.API.Queries
{
    public class GetTaskQuery : IRequest<TaskDto>
    {
        public int TaskId { get; set; }

        public GetTaskQuery(int taskId)
        {
            TaskId = taskId;
        }
    }
}
