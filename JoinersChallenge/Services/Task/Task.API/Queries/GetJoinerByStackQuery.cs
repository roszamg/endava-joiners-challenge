﻿using System.Collections.Generic;
using MediatR;
using Task.API.Models;

namespace Task.API.Queries
{
    public class GetJoinerByStackQuery : IRequest<List<ReportDto>>
    {        
        public string Stack { get; set; }
        public GetJoinerByStackQuery(string stack)
        {            
            Stack = stack;
        }
    }
}
