﻿namespace Task.API.Models
{
    public class ReportDto
    {
        public int JoinerIdentifier { get; set; }
        public string JoinerName { get; set; }
        public string Stack { get; set; }
        public string TaskName { get; set; }
        public string TaskDescription { get; set; }
        public int TaskEstimated { get; set; }
    }
}
