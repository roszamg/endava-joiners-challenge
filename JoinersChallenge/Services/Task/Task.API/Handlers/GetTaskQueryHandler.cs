﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Task.API.Models;
using Task.API.Queries;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.Handlers
{
    public class GetTaskQueryHandler : IRequestHandler<GetTaskQuery, TaskDto>
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetTaskQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<TaskDto> Handle(GetTaskQuery request, CancellationToken cancellationToken)
        {
            var task = await _unitOfWork.TaskRespository.GetTaskAsync(request.TaskId);

            return task == null ? null : _mapper.Map<TaskDto>(task);
        }
    }
}
