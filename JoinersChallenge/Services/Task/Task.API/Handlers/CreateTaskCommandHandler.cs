﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Task.API.Command;
using Task.API.Models;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.Handlers
{
    public class CreateTaskCommandHandler : IRequestHandler<CreateTaskCommand, TaskDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public CreateTaskCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public Task<TaskDto> Handle(CreateTaskCommand request, CancellationToken cancellationToken)
        {

            var task = _unitOfWork.TaskRespository.Add(new Domain.Task()
            {
                Name = request.Task.Name,
                Stack = request.Task.Stack,
                Description = request.Task.Description,
                EstimatedRequiereHours = request.Task.EstimatedRequiereHours,
                ParentTaskId = request.Task.ParentTaskId,
                JoinerId = request.Task.JoinerId
            });

            _unitOfWork.Commit();

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<TaskDto>(task));
        }
    }
}
