﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Task.API.Models;
using Task.API.Queries;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.Handlers
{
    public class GetTaskByJoinerQueryHandler : IRequestHandler<GetTaskByJoinerQuery, List<ReportDto>>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IMapper _mapper;

        public GetTaskByJoinerQueryHandler(IMapper mapper, ITaskRepository taskRepository)
        {
            _mapper = mapper;
            _taskRepository = taskRepository;
        }

        public async Task<List<ReportDto>> Handle(GetTaskByJoinerQuery request, CancellationToken cancellationToken)
        {
            var result = await _taskRepository.GetAsync(c => request.IdentificationNumber.HasValue ?
                                                        request.IdentificationNumber.Value == c.Joiner.IdentificationNumber :
                                                        c.Joiner.IdentificationNumber == c.Joiner.IdentificationNumber);

            return _mapper.Map<ICollection<Domain.Task>, List<ReportDto>>(result);
        }
    }
}
