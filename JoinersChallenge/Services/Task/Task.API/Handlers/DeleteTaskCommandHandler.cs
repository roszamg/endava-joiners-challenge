﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Task.API.Command;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.Handlers
{
    public class DeleteTaskCommandHandler : IRequestHandler<DeleteTaskCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;
        public DeleteTaskCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<bool> Handle(DeleteTaskCommand request, CancellationToken cancellationToken)
        {
            var task = await _unitOfWork.TaskRespository.GetTaskAsync(request.TaskId);

            if (task == null)
            {
                return false;
            }

            _unitOfWork.TaskRespository.Delete(task);

            _unitOfWork.Commit();
            return true;
        }
    }
}
