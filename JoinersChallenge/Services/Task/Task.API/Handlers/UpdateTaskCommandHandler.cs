﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Task.API.Command;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.Handlers
{
    public class UpdateTaskCommandHandler : IRequestHandler<UpdateTaskCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;        
        public UpdateTaskCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;            
        }


        public async Task<bool> Handle(UpdateTaskCommand request, CancellationToken cancellationToken)
        {
            var task = await _unitOfWork.TaskRespository.GetTaskAsync(request.TaskId);

            if (task == null)
                return false;

            task.Description = request.Task.Description;
            task.EstimatedRequiereHours = request.Task.EstimatedRequiereHours;
            task.JoinerId = request.Task.JoinerId;
            task.Name = request.Task.Name;
            task.ParentTaskId = request.Task.ParentTaskId;
            task.Stack = request.Task.Stack;

            _unitOfWork.Commit();

            return true;               

        }
    }
}
