﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Task.API.Models;
using Task.API.Queries;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.Handlers
{
    public class GetJoinerByStackQueryHandler : IRequestHandler<GetJoinerByStackQuery, List<ReportDto>>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IMapper _mapper;

        public GetJoinerByStackQueryHandler(ITaskRepository taskRepository, IMapper mapper)
        {
            _taskRepository = taskRepository;
            _mapper = mapper;
        }

        public async Task<List<ReportDto>> Handle(GetJoinerByStackQuery request, CancellationToken cancellationToken)
        {
            var result = await _taskRepository.GetAsync(c => c.Joiner.Stack.Contains(request.Stack));

            return _mapper.Map<List<Domain.Task>, List<ReportDto>>(result);
        }
    }
}
