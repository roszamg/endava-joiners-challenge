﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Task.API.Models;
using Task.API.Queries;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.Handlers
{
    public class GetAllTaskQueryHandler 
        : IRequestHandler<GetAllTaskQuery, List<TaskDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public GetAllTaskQueryHandler(IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<List<TaskDto>> Handle(GetAllTaskQuery request, CancellationToken cancellationToken)
        {
            var tasks = await _unitOfWork.TaskRespository.GetAllTaskAsync();

            return tasks == null ? null : _mapper.Map<List<TaskDto>>(tasks);
        }
    }
}
