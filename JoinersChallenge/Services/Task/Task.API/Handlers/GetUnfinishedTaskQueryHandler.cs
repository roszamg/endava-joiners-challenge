﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Task.API.Models;
using Task.API.Queries;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.Handlers
{
    public class GetUnfinishedTaskQueryHandler : IRequestHandler<GetUnfinishedTaskQuery, List<ReportDto>>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IMapper _mapper;
        public GetUnfinishedTaskQueryHandler(ITaskRepository taskRepository, IMapper mapper)
        {
            _taskRepository = taskRepository;
            _mapper = mapper;
        }
        public async Task<List<ReportDto>> Handle(GetUnfinishedTaskQuery request, CancellationToken cancellationToken)
        {
            var result = await _taskRepository.GetAsync(c => c.EstimatedRequiereHours != 0 && (request.JoinerIdentifier == null ? c.Joiner.IdentificationNumber > 0 : c.Joiner.IdentificationNumber == request.JoinerIdentifier));

            return _mapper.Map<List<Domain.Task>, List<ReportDto>>(result);
        }
    }
}
