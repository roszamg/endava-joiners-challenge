﻿using AutoMapper;
using Task.API.Models;

namespace Joiner.API.Mappers
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task.Domain.Task, TaskDto>();

            CreateMap<Task.Domain.Task, ReportDto>()
                .ForMember(x => x.JoinerIdentifier, x => x.MapFrom(y => y.Joiner.IdentificationNumber))
               .ForMember(x => x.JoinerName, x => x.MapFrom(y => y.Joiner.Name))
               .ForMember(x => x.Stack, x => x.MapFrom(y => y.Joiner.Stack))
               .ForMember(x => x.TaskDescription, x => x.MapFrom(y => y.Description))
               .ForMember(x => x.TaskEstimated, x => x.MapFrom(y => y.EstimatedRequiereHours))
               .ForMember(x => x.TaskName, x => x.MapFrom(y => y.Name));
        }
    }
}
