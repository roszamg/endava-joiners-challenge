﻿using AutoMapper;
using Task.API.Models;

namespace Task.API.Mappers
{
    public class TaskToReportProfile : Profile
    {
        public TaskToReportProfile()
        {
            CreateMap<Domain.Task, ReportDto>()
                .ForMember(x => x.JoinerName, x => x.MapFrom(y => y.Joiner.Name))
                .ForMember(x => x.TaskDescription, x => x.MapFrom(y => y.Description))
                .ForMember(x => x.TaskEstimated, x => x.MapFrom(y => y.EstimatedRequiereHours))
                .ForMember(x => x.TaskName, x => x.MapFrom(y => y.Name));
        }

    }
}
