﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Task.API.Command;
using Task.API.Models;
using Task.API.Queries;

namespace Task.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<TaskController> _logger;
        public TaskController(IMediator mediator, ILogger<TaskController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet("{taskId}")]
        public async Task<IActionResult> Get(int taskId)
        {
            _logger.LogInformation("Get Task information");

            var query = new GetTaskQuery(taskId);
            var result = await _mediator.Send(query);

            return result != null ? (IActionResult)Ok(result) : NotFound();
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllTask()
        {
            _logger.LogInformation("Get All Task information");

            var query = new GetAllTaskQuery();
            var result = await _mediator.Send(query);

            return result != null ? (IActionResult)Ok(result) : NotFound();
        }

        [HttpGet("[action]/{stack}")]
        public async Task<List<ReportDto>> GetTaskJoinerByStack(string stack)
        {
            _logger.LogInformation("Get Task Joiner by Stack");

            var query = new GetJoinerByStackQuery(stack);
            var result = await _mediator.Send(query);

            return result;
        }

        [Route("[action]/joinerIdentifier")]
        [HttpGet()]
        public async Task<List<ReportDto>> GetTaskByJoiner(int? joinerIdentifier = null)
        {
            _logger.LogInformation("Get Task by Joiner");

            var query = new GetTaskByJoinerQuery(joinerIdentifier);
            var result = await _mediator.Send(query);

            return result;
        }

        [HttpGet("[action]/joinerIdentifier")]
        public async Task<List<ReportDto>> GetUnfinishedTaskByJoiner(int? joinerIdentifier = null)
        {
            _logger.LogInformation("Get Unfinished Task By Joiner");

            var query = new GetUnfinishedTaskQuery(joinerIdentifier);
            var result = await _mediator.Send(query);

            return result;
        }

        [HttpPost("CreateTask")]
        public async Task<IActionResult> Post([FromBody] TaskDto taskDto)
        {
            _logger.LogInformation("Create Task");

            var query = new CreateTaskCommand(taskDto);
            var result = await _mediator.Send(query);

            return CreatedAtAction(nameof(Post), result);
        }

        [HttpPut("UpdateTask/{taskId}")]
        public async Task<IActionResult> Put(int taskId, [FromBody] TaskDto taskDto)
        {

            _logger.LogInformation("Update  Task");

            var query = new UpdateTaskCommand(taskDto, taskId);
            var result = await _mediator.Send(query);

            return CreatedAtAction(nameof(Put), result);
        }

        [HttpDelete("{taskId}")]
        public async Task<IActionResult> Delete(int taskId)
        {
            _logger.LogInformation("Delete  Task");

            var query = new DeleteTaskCommand(taskId);
            var result = await _mediator.Send(query);

            return result ? (IActionResult)Ok() : NotFound();
        }

    }
}
