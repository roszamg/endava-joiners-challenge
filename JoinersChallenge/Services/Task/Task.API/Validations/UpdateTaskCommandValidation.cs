﻿using FluentValidation;
using Task.API.Command;
using Task.Persistence.DataBase.Repositories;

namespace Task.API.Validations
{
    public class UpdateTaskCommandValidation : AbstractValidator<UpdateTaskCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        
        public UpdateTaskCommandValidation(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

            RuleFor(x => x.Task.Name).NotEmpty();
            When(x => x.Task.ParentTaskId.HasValue && x.Task.ParentTaskId.Value != 0, () =>
            {
                RuleFor(x => x.Task.ParentTaskId).Must(ExistParentTask).WithMessage("The parent task does not exist");
                RuleFor(x => x.Task.ParentTaskId).Must(IsOneLevel).WithMessage("Should be one level relationship");
            });

            RuleFor(x => x.Task.JoinerId).Must(ExistJoiner).WithMessage("The joiner does not exist");

        }

        private bool ExistParentTask(int? parentTaskdId)
        {
            var task = _unitOfWork.TaskRespository.GetTaskAsync(parentTaskdId.Value);
            task.Wait();            

            return task.Result != null;
        }

        private bool IsOneLevel(int? parentTaskdId)
        {
            var task = _unitOfWork.TaskRespository.GetTaskAsync(parentTaskdId.Value);
            task.Wait();

            return !task.Result.ParentTaskId.HasValue;
        }

        private bool ExistJoiner(int joinerId)
        {
            var joiner = _unitOfWork.JoinerRepository.GetJoinerAsync(joinerId);
            joiner.Wait();

            return joiner.Result != null;
        }

    }
}
