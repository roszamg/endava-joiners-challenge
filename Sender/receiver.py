import time
import awsSqsHelper as sqs
import requests
import json
import os

def process_message(message_body):    
    url =   "http://localhost:52793/gateway/joiner"      
    headers = {'Content-Type': 'application/json'}
    
    resp = requests.post(url, headers=headers, data=message_body)   
  
    if resp.status_code != 201:
        print("aqui")
        print (resp.status_code)
        sqs.send_error_queue(message_body)
        return           
    print(resp.json())    

if __name__ == "__main__":
    while True:
        response = sqs.recive_message()     

        if not any(response.get("Messages", [])):
            print('sleep 60 sec')
            time.sleep(60)
            print('continue')            

        for message in response.get("Messages", []):       
            process_message(message["Body"])
            sqs.delete_message(message['ReceiptHandle'])
