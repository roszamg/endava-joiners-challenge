import boto3
import json 
import os

sqs = boto3.client('sqs')
sqsUrl= 'https://sqs.us-east-1.amazonaws.com/247463976255/joiners'
sqsErrorUrl = 'https://sqs.us-east-1.amazonaws.com/247463976255/joiner_error'

def send_messages(joiner):        
    response = sqs.send_message(
        QueueUrl = sqsUrl,
        DelaySeconds = 10,
        MessageBody = json.dumps(joiner)
    )

    print(response['MessageId'])
     

def recive_message():
    response = sqs.receive_message(
        QueueUrl = sqsUrl,
        MaxNumberOfMessages =1,
        WaitTimeSeconds=10
    )

    return response

def delete_message(receipthandle):
    print(f'Deleting Message : {receipthandle}')
    sqs.delete_message(
          QueueUrl = sqsUrl,
          ReceiptHandle=receipthandle
          )

def send_error_queue(joiner):
    response = sqs.send_message(
        QueueUrl = sqsErrorUrl,
        DelaySeconds = 10,
        MessageBody = json.dumps(joiner)
    )
    print(response['MessageId'])