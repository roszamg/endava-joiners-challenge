from flask import Flask
from flask_restful import Api, Resource, reqparse
from flask_restful_swagger import swagger
import werkzeug
from werkzeug.utils import secure_filename
import os

import pdfplumber
import re
import awsSqsHelper as sqs 

ALLOWED_EXTENSIONS = {'pdf', 'docx'}
UPLOAD_FOLDER = os.path.dirname(os.path.abspath(__file__)) + '/uploads/' 

application = app = Flask(__name__)
api = Api(app)


app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

joiner_post_args = reqparse.RequestParser()
joiner_post_args.add_argument("filename", type=werkzeug.datastructures.FileStorage ,help="File  is required", required=True , location='files' )


class Joiner(Resource):
    def post(self):      
        try:
            args = joiner_post_args.parse_args()  
        
            if not allowed_file(args['filename'].filename): 
                return {"Error": "The document type is not allowed."}

            file = args['filename']    
            filename = secure_filename(args['filename'].filename)
            path = os.path.join(app.config['UPLOAD_FOLDER'] + filename)

            file.save(path)  
            file.close()  
                            
            joineroutput = readpdf(path)
            sqs.send_messages(joineroutput)
            os.remove(path)
            return { "fileName": args['filename'].filename, "Joiner":joineroutput }
        except Exception as e:
            return { "MessageError": str(e) }             

def allowed_file(filename):
       return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS 

def readpdf(file_path):
    with pdfplumber.open(file_path) as pdf:
        pageobj = pdf.pages[0]
        text = pageobj.extract_text()            
        outputv = searchtext(text) 
        return   outputv

def searchtext(text):   
    m = re.search(r'Name:\s+(?P<name>.*)\s+Role:\s+(?P<lastname>.*)', text, re.MULTILINE)
    m1 = re.search(r'Role:\s+(?P<role>.*)', text, re.MULTILINE)
    m2 =re.search(r'Languages:(?P<lan>.*),?', text, re.MULTILINE)
    m3 = re.search(r'Domain\sExperience\s+(?s)(?P<domain>(?:[^\n][\n]?)+)\s+Core\sTechnical\sSkills', text, re.MULTILINE)
    m4 = re.search(r'Core\sTechnical\sSkills\s+(?s)(?P<stack>(?:[^\n][\n]?)+)\s+Education\sand\sTraining', text, re.MULTILINE)
    m5 = re.search(r'Identification\sNumber:\s*(?P<ID>.*),?', text, re.MULTILINE)
    if m:        
        name = m.group('name')
    if m1:            
        role =m1.group('role')
    if m2:            
        lan =m2.group('lan')
    if m3:            
        domainexp =m3.group('domain').replace('\n','')
    if m4:            
        stack =m4.group('stack').replace('\n','')
    if m5:
        identificationnumber = int(m5.group('ID'))
    return {'joinerId':0,'name': name, 'identificationNumber':identificationnumber,'stack': stack, 'role':role, 'language': lan, 'domainExperience': domainexp}
    

api.add_resource(Joiner, "/sender")

if __name__ == "__main__":
    app.run(debug=True)